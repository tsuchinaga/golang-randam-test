package main

import (
	"math/rand"
	"reflect"
	"testing"
)

func TestAgent_BestAction(t *testing.T) {
	testTable := []struct {
		qValue         map[Action]float64
		expectContains []Action
		times          int
	}{
		{map[Action]float64{Up: 1, Right: 0.5, Down: 0.5, Left: 0.5}, []Action{Up}, 1},
		{map[Action]float64{Up: 0.5, Right: 1, Down: 0.5, Left: 0.5}, []Action{Right}, 1},
		{map[Action]float64{Up: 0.5, Right: 0.5, Down: 1, Left: 0.5}, []Action{Down}, 1},
		{map[Action]float64{Up: 0.5, Right: 0.5, Down: 0.5, Left: 1}, []Action{Left}, 1},
		{map[Action]float64{Up: 1, Right: 1, Down: 0.5, Left: 0.5}, []Action{Up, Right}, 10},
		{map[Action]float64{Up: 1, Right: 0.5, Down: 1, Left: 0.5}, []Action{Up, Down}, 10},
		{map[Action]float64{Up: 1, Right: 0.5, Down: 0.5, Left: 1}, []Action{Up, Left}, 10},
	}

	for i, test := range testTable {
		agent := Agent{QValue: test.qValue}
		actual := agent.BestAction()

		for j := 1; j <= test.times; j++ {
			result := false
			for _, action := range test.expectContains {
				if actual == action {
					result = true
				}
			}

			if !result {
				t.Fatalf("%s No.%d-%d 失敗\n期待: %+v(%+v)\n実際: %+v(%+v)",
					t.Name(), i+1, j, test.expectContains, reflect.TypeOf(test.expectContains), actual, reflect.TypeOf(actual))
			}
		}
	}
}

func TestAgent_ChoiceAction(t *testing.T) {
	agent := Agent{}
	testTable := []struct {
		r      float64
		expect Action
	}{
		{0, Up},
		{0.24999, Up},
		{0.25, Right},
		{0.49999, Right},
		{0.5, Down},
		{0.74999, Down},
		{0.75, Left},
		{0.99999, Left},
		{-1, Error},
		{1, Error},
	}

	for i, test := range testTable {
		actual := agent.ChoiceAction(test.r)
		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗\n期待: %+v(%+v)\n実際: %+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}
	}
}

func TestAgent_IsEpsilon(t *testing.T) {
	agent := Agent{}

	testTable := []struct {
		e      float64
		r      float64
		expect bool
	}{
		{0, 0, false},
		{1, 0, true},
		{1, 1, false},
		{0.5, 0, true},
		{0.5, 0.49, true},
		{0.5, 0.5, false},
		{0.5, 0.99, false},
	}

	for i, test := range testTable {
		agent.Epsilon = test.e
		actual := agent.IsEpsilon(test.r)
		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗\n期待: %+v(%+v)\n実際: %+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}
	}
}

func Test_RandFloat64(t *testing.T) {
	testTable := []struct {
		seed    int64
		expects []float64
	}{
		{9, []float64{0.00364701337293379, 0.10120121767574507, 0.5225728931671818}},
		{1, []float64{0.6046602879796196, 0.9405090880450124, 0.6645600532184904}},
		{1257894000000000001, []float64{0.059473619663795774, 0.9222174892883567, 0.32041802949553816}},
		{1257894000000000002, []float64{0.5902283988800163, 0.24727712791963616, 0.7073467764896421}},
	}

	for i, test := range testTable {
		rand.Seed(test.seed)

		for j, expect := range test.expects {
			actual := rand.Float64()
			if expect != actual {
				t.Fatalf("%s No.%d-%d 失敗\n期待: %+v(%+v)\n実際: %+v(%+v)",
					t.Name(), i+1, j+1, expect, reflect.TypeOf(expect), actual, reflect.TypeOf(actual))
			}
		}
	}
}

func TestAgent_GetAction(t *testing.T) {
	testTable := []struct {
		agent  Agent
		expect Action
	}{
		{Agent{Epsilon: 0.1, RandSeed: 9}, Up},
		{Agent{Epsilon: 0.1, RandSeed: 11}, Left},
		{Agent{Epsilon: 0.1, RandSeed: 18}, Down},
		{Agent{Epsilon: 0.1, RandSeed: 86}, Right},
		{Agent{Epsilon: 0.1, RandSeed: 100, QValue: map[Action]float64{Up: 1, Right: 0.5, Down: 0.5, Left: 0.5}}, Up},
		{Agent{Epsilon: 0.1, RandSeed: 100, QValue: map[Action]float64{Up: 0.5, Right: 1, Down: 0.5, Left: 0.5}}, Right},
		{Agent{Epsilon: 0.1, RandSeed: 100, QValue: map[Action]float64{Up: 0.5, Right: 0.5, Down: 1, Left: 0.5}}, Down},
		{Agent{Epsilon: 0.1, RandSeed: 100, QValue: map[Action]float64{Up: 0.5, Right: 0.5, Down: 0.5, Left: 1}}, Left},
	}

	for i, test := range testTable {
		agent := test.agent
		actual := agent.GetAction()
		if test.expect != actual {
			t.Fatalf("%s No.%d 失敗\n期待: %+v(%+v)\n実際: %+v(%+v)",
				t.Name(), i+1, test.expect, reflect.TypeOf(test.expect), actual, reflect.TypeOf(actual))
		}
	}
}
