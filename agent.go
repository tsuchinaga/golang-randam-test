package main

import (
	"math"
	"math/rand"
)

type Action int

const (
	Up Action = iota
	Right
	Down
	Left
	Error Action = -1
)

type Agent struct {
	QValue   map[Action]float64
	Epsilon  float64
	RandSeed int64
}

func (a Agent) GetAction() Action {
	rand.Seed(a.RandSeed)
	action := Error

	if a.IsEpsilon(rand.Float64()) { // ランダム選択
		action = a.ChoiceAction(rand.Float64())
	} else { // 最適選択
		action = a.BestAction()
	}

	return action
}

func (a Agent) IsEpsilon(r float64) bool {
	if 0 <= r && r < a.Epsilon {
		return true
	}

	return false
}

func (a Agent) ChoiceAction(r float64) Action {
	action := Error

	aNum := math.Floor(r * 4)
	if 0 <= aNum && aNum < 4 {
		action = Action(aNum)
	}

	return action
}

func (a Agent) BestAction() Action {
	action := Error
	max := -1.0

	for k, v := range a.QValue {
		if max < v {
			action = k
			max = v
		}
	}

	return action
}
